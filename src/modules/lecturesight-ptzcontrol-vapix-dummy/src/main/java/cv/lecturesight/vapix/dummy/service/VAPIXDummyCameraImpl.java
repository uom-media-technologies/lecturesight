package cv.lecturesight.vapix.dummy.service;

import cv.lecturesight.ptz.api.CameraListener;
import cv.lecturesight.ptz.api.PTZCamera;
import cv.lecturesight.ptz.api.PTZCameraException;
import cv.lecturesight.ptz.api.PTZCameraProfile;
import cv.lecturesight.util.conf.Configuration;
import cv.lecturesight.util.geometry.Position;
import cv.lecturesight.util.geometry.Preset;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.osgi.service.component.ComponentContext;
import org.pmw.tinylog.Level;
import org.pmw.tinylog.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * VAPIX® is Axis’ own open API (Application Programming Interface) using
 * standard protocols enabling integration into a wide range of solutions on
 * different platforms. Almost all functionality available in Axis products can
 * be controlled using VAPIX®. VAPIX® is continuously developed and the main
 * interface to our products.
 * http://www.axis.com/techsup/cam_servers/dev/cam_http_api_index.php
 *
 * The communication with the camera is based around Hypertext Transfer Protocol
 * (HTTP) response and requests.
 * The returning value for success is:
 *
 * - HTTP_NO_CONTENT (204): Command has been sent.
 * - HTTP_OK (200): Command sent.
 *
 * and response in text format. The returning text format is structured
 * as [propertyName]=[propertyValue]
 *
 * @author Corne Oosthuizen (CILT - UCT) [2015-12]
 */
@Component(name = "cv.lecturesight.vapix", immediate = true)
@Service
public class VAPIXDummyCameraImpl implements PTZCamera {

  @Reference
  Configuration config; // service configuration

  String model_name;
  String brand;
  String host;
  String username;
  String password;

  // Is camera inverted
  boolean inverted = false;

  public Limits lim_pan;
  public Limits lim_tilt;
  public Limits lim_zoom;
  Limits speed_pan;
  Limits speed_tilt;
  Limits speed_zoom;
  public Limits range_pan;
  public Limits range_tilt;
  public Limits range_zoom;
  public Limits range_focus;

  // Scale values are used to scale inputs to corrected output values
  float scale_pan = 0f;
  float scale_tilt = 0f;
  float scale_zoom = 0f;
  float scale_pan_rev = 0f;
  float scale_tilt_rev = 0f;
  float scale_zoom_rev = 0f;
  float scale_pan_speed = 0f;
  float scale_tilt_speed = 0f;
  int zoom;
  int focus;

  boolean can_move_absolute = false;
  boolean can_move_relative = false;
  boolean can_move_continuous = false;
  boolean flipped = false;

  Properties properties;
  PTZCameraProfile profile;
  List<CameraListener> observers;
  List<Preset> presets;

  long last_update; // time in milliseconds of last position update
  int updateInterval = 200; // number of millisec. between state updates

  private final ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
  ScheduledFuture updaterHandle;

  final Position HOME_POS = new Position(0, 0);
  boolean running = false;
  Position current_pos = HOME_POS.clone();
  Position target_pos = HOME_POS.clone();
  String current_mode = "continuous";
  int current_zoom = 0;
  int target_zoom = 0;
  int current_focus = 0;
  int current_panSpeed = 0;
  int current_tiltSpeed = 0;

  final Object mutex = new Object();

  /**
   * Service Component Runtime uses default constructor, so the implicite
   * default constructor suffices.
   */

  /**
   * OSGI service activation method.
   *
   * Initialize this ONVIF Camera service object and load all the appropriate
   * details from properties.
   *
   * @param cc
   *            ComponentContext
   * @throws cv.lecturesight.ptz.api.PTZCameraException
   */
  protected void activate(ComponentContext cc) throws PTZCameraException {
    this.last_update = 0L;

    // Load properties
    host = config.get(Constants.PROPKEY_VAPIX_CAMERA);
    username = config.get(Constants.PROPKEY_VAPIX_USERNAME);
    password = config.get(Constants.PROPKEY_VAPIX_PASSWORD);

    inverted = config.getBoolean(Constants.PROPKEY_INVERTED);

    updateInterval = config.getInt(Constants.PROPKEY_UPDATER_INTERVAL);

    lim_pan = new Limits(config.getInt(Constants.PROFKEY_PAN_MIN), config.getInt(Constants.PROFKEY_PAN_MAX));
    lim_tilt = new Limits(config.getInt(Constants.PROFKEY_TILT_MIN), config.getInt(Constants.PROFKEY_TILT_MAX));
    lim_zoom = new Limits(config.getInt(Constants.PROFKEY_ZOOM_MIN), config.getInt(Constants.PROFKEY_ZOOM_MAX));

    speed_pan = new Limits(0, config.getInt(Constants.PROFKEY_PAN_MAXSPEED));
    speed_tilt = new Limits(0, config.getInt(Constants.PROFKEY_TILT_MAXSPEED));
    speed_zoom = new Limits(0, config.getInt(Constants.PROFKEY_ZOOM_MAXSPEED));

    if (host.length() > 0) {

      this.model_name = "VAPIX Camera (404)";
      this.brand = "None";
      this.profile = new PTZCameraProfile(this.brand, // manufacturer
          this.model_name, // should be "VAPIX Camera (404)"
          0, 0, 0, // pan min, max, speed
          0, 0, 0, // tilt min, max, speed
          0, 0, 0, // zoom min, max, speed
          new Position(0, 0));

      Map<String, String> result = processCommand("/axis-cgi/param.cgi?action=list&group=Brand");
      Logger.info("Vapix connecting to " + host + " [ " + username + " : " + password + " ] ("
          + result.get("success") + ")");

      if (result.get("success").equals("1")) {
        this.model_name = "AXIS V5915 PTZ Network Camera";
        this.brand = "AXIS";

        this.presets = getPresets();

        Logger.info("Vapix: " + this.brand + " " + this.model_name);

        Map<String, String> parameters = processCommand("/axis-cgi/param.cgi?action=list&group=PTZ");

        if (parameters.get("success").equals("1")) {
          can_move_absolute = true;
          can_move_relative = true;
          can_move_continuous = true;

          range_pan = new Limits(-170,170);

          if (inverted) {
            range_tilt = new Limits(-90,20);
          } else {
            range_tilt = new Limits(-20,90);
          }

          range_zoom = new Limits(1,9999);
          range_focus = new Limits(770,9999);

          this.scale_pan = (float) (range_pan.max() - range_pan.min()) / (float) (lim_pan.max() - lim_pan.min());
          this.scale_tilt = (float) (range_tilt.max() - range_tilt.min()) / (float) (lim_tilt.max() - lim_tilt.min());
          this.scale_zoom = (float) (range_zoom.max() - range_zoom.min()) / (float) (lim_zoom.max() - lim_zoom.min());

          this.scale_pan_rev = (float) (lim_pan.max() - lim_pan.min())
              / (float) (range_pan.max() - range_pan.min());
          this.scale_tilt_rev = (float) (lim_tilt.max() - lim_tilt.min())
              / (float) (range_tilt.max() - range_tilt.min());
          this.scale_zoom_rev = (float) (lim_zoom.max() - lim_zoom.min())
              / (float) (range_zoom.max() - range_zoom.min());

          // documentation for pan and tilt speed is 100 for max speed
          this.scale_pan_speed = (float) (100 - 0) / (float) (speed_pan.max() - speed_pan.min());
          this.scale_tilt_speed = (float) (100 - 0) / (float) (speed_tilt.max() - speed_tilt.min());

          Logger.trace("Profile: " + "Pan: " + lim_pan + " " + speed_pan + " " + range_pan + " | Tilt: "
              + lim_tilt + " " + speed_tilt + " " + range_tilt + " | Zoom: " + lim_zoom + " " + speed_zoom
              + " " + range_zoom);

          Logger.trace(" Delta: " + scale_pan + " " + scale_tilt + " " + scale_zoom + " | " + scale_pan_rev
              + " " + scale_tilt_rev + " " + scale_zoom_rev + " | " + scale_pan_speed + " "
              + scale_tilt_speed);

          this.profile = new PTZCameraProfile(this.brand, this.model_name, lim_pan.min(), lim_pan.max(),
              speed_pan.max(), lim_tilt.min(), lim_tilt.max(), speed_tilt.max(), lim_zoom.min(), lim_zoom.max(),
              speed_zoom.max(), getPosition());

          this.observers = new LinkedList<>();

          // start update thread
          running = true;
          updaterHandle = executor.scheduleWithFixedDelay(new CameraStateUpdater(), updateInterval,
              updateInterval, TimeUnit.MILLISECONDS);
        } else {
          String msg = "Camera not responding to HTTPRequest - group=PTZ.";
          Logger.debug(msg);
          throw new PTZCameraException(msg);
        }
      } else {
        String msg = "Camera not responding to HTTPRequest - group=Brand.";
        Logger.debug(msg);
        throw new PTZCameraException(msg);
      }

    } else {
      String msg = "Could not connect to VAPIX Camera - no host name";
      Logger.error(msg);

      // OSGI-Hint: In case this service is not able initialize its
      // functionality it must to throw an Exception
      // to signal to the OSGI-framework that the service activation has
      // failed.
      throw new PTZCameraException(msg);
    }

  }

  /**
   * OSGI service de-activation method.
   *
   * @param cc
   *            Context of component in OSGI
   */
  protected void deactivate(ComponentContext cc) {

    // de-init
    try {
      executor.shutdown();
      executor.awaitTermination(1, TimeUnit.SECONDS);
    } catch (Exception e) {
      Logger.debug("Unable to terminate scheduled processes cleanly");
    }
    running = false;
    Logger.debug("Deactivated");
  }

  /**
   * The url connection for the camera
   *
   * @return string with url to send HTTP request to
   */
  public String getUrl() {
    return "http://" + this.host;
  }

  /**
   * Send the command to the camera and handles response
   *
   * @param command
   *            The command to execute
   * @return String containing the resulting string of executing the command
   */
  public String doCommand(String command) throws IOException {

    Logger.debug(getUrl() + command);
    return "ok";
  }

  /**
   * The response in most cases is a list of properties and is then put in a
   * Hashtable for easy access
   *
   * @param command
   *            The command to execute
   * @return Hastable structure with result of command as <propertyName,
   *         propertyValue>
   */
  public Map<String, String> processCommand(String command) {

    // In a dummy camera everything is a success
    Map<String, String> processed = new HashMap<>();
    processed.put("success", "1");

    return processed;
  }

  /**
   * Return the name of the ONVIF camera
   *
   * @return String containing the model name of this camera
   */
  @Override
  public String getName() {
    return this.model_name;
  }

  /**
   * Return the camera profile object
   *
   * @return Camera profile
   */
  @Override
  public PTZCameraProfile getProfile() {
    return this.profile;
  }

  /**
   * This function is not implemented, because actual usage not defined in any
   * documentation
   */
  @Override
  public void reset() {
    Logger.debug("Reset (do nothing)");
  }

  /**
   * Cancel the current movement - calls stopMove()
   */
  @Override
  public void cancel() {
    this.stopMove();
  }

  /**
   * Stops the current camera movement if any is in progress
   */
  @Override
  public void stopMove() {
    Logger.debug("Stop movement");

    synchronized(mutex) {
      target_pos = current_pos.clone();
      target_zoom = current_zoom;
    }
  }

  /**
   * Move the camera to home position - preset (home)
   */
  @Override
  public void moveHome() {

    Logger.debug("Move home");

    synchronized(mutex) {
      target_pos = HOME_POS.clone();
      current_panSpeed = 50;
      current_tiltSpeed = 50;
    }
  }

  /**
   * Move the camera to a set preset in the preset list using the index of the
   * preset (0 index)
   *
   * @param preset_index
   *            The number of the preset in the preset list to move camera to
   */
  @Override
  public void movePreset(int preset_index) {
    Logger.debug("Move preset index " + preset_index);
    moveHome();
  }

  /**
   * Move the camera to a set preset using the name of that preset
   *
   * @param preset
   *            The name of the preset to move the camera to
   */
  public void movePreset(String preset) {
    Logger.debug("Move preset name " + preset);
    moveHome();
  }

  /**
   * Set a preset at the current camera position with the provided name
   *
   * @param name
   *            Name of the preset to store current position
   */
  public void setPreset(String name) {

    try {
      String result = doCommand("/axis-cgi/com/ptzconfig.cgi?setserverpresetname=" + name);
      Logger.trace("setPreset: " + name + "(" + result + ")");
      this.presets = getPresets(); // refresh the list of presets
    } catch (IOException e) {
      Logger.error("setPreset: " + e.getMessage());
    } finally {
    }
  }

  /**
   * Remove a preset by name
   *
   * @param name
   *            Remove a preset with the given name from the camera
   */
  public void removePreset(String name) {

    try {
      String result = doCommand("/axis-cgi/com/ptzconfig.cgi?removeserverpresetname=" + name);
      Logger.trace("removePreset: " + name + "(" + result + ")");
      this.presets = getPresets(); // refresh the list of presets
    } catch (Exception e) {
      Logger.error("removePreset: " + e.getMessage());
    }
  }

  /**
   * Get a list of all the available presets for this camera
   *
   * @return String array of all the presets on the camera
   */
  @Override
  public List<Preset> getPresets() {
    ArrayList<Preset> presetList = new ArrayList<>();
    presetList.add(new Preset("home", 0, 0, 100));
    presetList.add(new Preset("pos1", -2000, -500, 100));
    presetList.add(new Preset("pos2", -1000, 0, 100));
    presetList.add(new Preset("pos3", 1000, 500, 100));
    presetList.add(new Preset("pos4", 2000, 1000, 100));

    return presetList;
  }

  /**
   * Move the camera in a direction using the set speeds
   *
   * @param panSpeed
   *            speed to pan camera (-100, 100)
   * @param tiltSpeed
   *            speed to tilt camera at (-100, 100)
   */
  public void moveCamera(int panSpeed, int tiltSpeed) {
    if (this.can_move_continuous) {

      /* Cap LS speed to VAPIX Speed
      if (panSpeed < -100) {
        panSpeed = -100;
      }

      if (panSpeed > 100) {
        panSpeed = 100;
      }

      if (tiltSpeed < -100) {
        tiltSpeed = -100;
      }

      if (tiltSpeed > 100) {
        tiltSpeed = 100;
      } */
      synchronized(mutex) {
        current_mode = "continuous";
        current_panSpeed = panSpeed;
        current_tiltSpeed = tiltSpeed;
      }

      Logger.trace("Move " + panSpeed + ", " + tiltSpeed);

    } else {
      this.moveNotSupported(tiltSpeed);
    }

  }

  /**
   * Tilt the camera up at tiltSpeed
   *
   * @param tiltSpeed
   *            The speed of the tilt motion
   */
  @Override
  public void moveUp(int tiltSpeed) {
    synchronized(mutex) {
      target_pos.setX(current_pos.getX());
      target_pos.setY(lim_tilt.max());
    }

    moveCamera(0,tiltSpeed);
  }

  /**
   * Tilt the camera down at tiltSpeed
   *
   * @param tiltSpeed
   *            The speed of the tilt motion
   */
  @Override
  public void moveDown(int tiltSpeed) {
    synchronized(mutex) {
      target_pos.setX(current_pos.getX());
      target_pos.setY(lim_tilt.min());
    }

    moveCamera(0, -tiltSpeed);
  }

  /**
   * Pan the camera left at panSpeed
   *
   * @param panSpeed
   *            The speed of the panning motion
   */
  @Override
  public void moveLeft(int panSpeed) {
    synchronized(mutex) {
      target_pos.setX(lim_pan.min());
      target_pos.setY(current_pos.getY());
    }

    moveCamera(-panSpeed, 0);
  }

  /**
   * Pan the camera right at panSpeed
   *
   * @param panSpeed
   *            The speed of the panning motion
   */
  @Override
  public void moveRight(int panSpeed) {
    synchronized(mutex) {
      target_pos.setX(lim_pan.max());
      target_pos.setY(current_pos.getY());
    }

    moveCamera(panSpeed, 0);
  }

  /**
   * Simultaneously pan and tilt the camera Up and Left at panSpeed and
   * tiltSpeed respectively
   *
   * @param panSpeed
   *            The speed of the panning motion
   * @param tiltSpeed
   *            The speed of the tilt motion
   */
  @Override
  public void moveUpLeft(int panSpeed, int tiltSpeed) {
    synchronized(mutex) {
      target_pos.setX(lim_pan.min());
      target_pos.setY(lim_tilt.max());
    }

    moveCamera(-panSpeed, tiltSpeed);
  }

  /**
   * Simultaneously pan and tilt the camera Up and Right at panSpeed and
   * tiltSpeed respectively
   *
   * @param panSpeed
   *            The speed of the panning motion
   * @param tiltSpeed
   *            The speed of the tilt motion
   */
  @Override
  public void moveUpRight(int panSpeed, int tiltSpeed) {
    synchronized(mutex) {
      target_pos.setX(lim_pan.max());
      target_pos.setY(lim_tilt.max());
    }

    moveCamera(panSpeed, tiltSpeed);
  }

  /**
   * Simultaneously pan and tilt the camera Down and Left at panSpeed and
   * tiltSpeed respectively
   *
   * @param panSpeed
   *            The speed of the panning motion
   * @param tiltSpeed
   *            The speed of the tilt motion
   */
  @Override
  public void moveDownLeft(int panSpeed, int tiltSpeed) {
    synchronized(mutex) {
      target_pos.setX(lim_pan.min());
      target_pos.setY(lim_tilt.min());
    }

    moveCamera(-panSpeed, -tiltSpeed);
  }

  /**
   * Simultaneously pan and tilt the camera Down and Right at panSpeed and
   * tiltSpeed respectively
   *
   * @param panSpeed
   *            The speed of the panning motion
   * @param tiltSpeed
   *            The speed of the tilt motion
   */
  @Override
  public void moveDownRight(int panSpeed, int tiltSpeed) {
    synchronized(mutex) {
      target_pos.setX(lim_pan.max());
      target_pos.setY(lim_tilt.min());
    }

    moveCamera(panSpeed, -tiltSpeed);
  }

  /**
   * Move to an absolute position - target to move to and the speed to use.
   * VAPIX does not support separate speed setting for pan and tilt so
   * panSpeed is used.
   *
   * @param panSpeed
   *            The speed of the panning motion
   * @param tiltSpeed
   *            The speed of the tilt motion
   * @param target
   *            Target to move camera to at the provided speed
   */
  @Override
  public void moveAbsolute(int panSpeed, int tiltSpeed, Position target) {
    if (this.can_move_absolute) {
      current_mode = "absolute";
      moveToTarget(panSpeed, tiltSpeed, target);
    } else {
      this.moveNotSupported(panSpeed, tiltSpeed, target);
    }
  }

  /**
   * Move relative position - target to move to and the speed to use. VAPIX
   * does not support separate speed setting for pan and tilt so panSpeed is
   * used.
   *
   * @param panSpeed
   *            The speed of the panning motion
   * @param tiltSpeed
   *            The speed of the tilt motion
   * @param target
   *            Target to move camera to at the provided speed
   */
  @Override
  public void moveRelative(int panSpeed, int tiltSpeed, Position target) {
    if (this.can_move_relative) {
       Position abs_target = new Position(target.getX() + current_pos.getX(),
               target.getY() + current_pos.getY());
       current_mode = "relative";
       moveToTarget(panSpeed, tiltSpeed, abs_target);
    } else {
      this.moveNotSupported(panSpeed, tiltSpeed, target);
    }
  }

  void moveToTarget(int panSpeed, int tiltSpeed, Position target) {

    target_pos = target.clone();
    // float z = this.map((int) this.zoom, lim_zoom.min(), scale_zoom, range_zoom.min()());
    // The VAPIX protocol only has one speed setting for both pan an
    // tilt - so the pan speed is used
    current_panSpeed = panSpeed;
    current_tiltSpeed = panSpeed;
  }

  /**
   * Clear the camera limits and reset to provided properties
   */
  @Override
  public void clearLimits() {

    Logger.trace("Clear camera limits");
    lim_pan = new Limits(config.getInt(Constants.PROFKEY_PAN_MIN), config.getInt(Constants.PROFKEY_PAN_MAX));
    lim_tilt = new Limits(config.getInt(Constants.PROFKEY_TILT_MIN), config.getInt(Constants.PROFKEY_TILT_MAX));
    lim_zoom = new Limits(config.getInt(Constants.PROFKEY_ZOOM_MIN), config.getInt(Constants.PROFKEY_ZOOM_MAX));

    speed_pan = new Limits(0, config.getInt(Constants.PROFKEY_PAN_MAXSPEED));
    speed_tilt = new Limits(0, config.getInt(Constants.PROFKEY_TILT_MAXSPEED));
    speed_zoom = new Limits(0, config.getInt(Constants.PROFKEY_ZOOM_MAXSPEED));
  }

  @Override
  public void setLimitUpRight(int pan, int tilt) {

    // TODO
    Logger.debug("Not Implemented: Set limit up-right");

  }

  @Override
  public void setLimitDownLeft(int pan, int tilt) {

    // TODO
    Logger.debug("Not Implemented: Set limit down-left");

  }

  /**
   * Stop the camera zoom operation
   */
  @Override
  public void stopZoom() {

    try {

      if (this.can_move_continuous) {

        String result = doCommand("/axis-cgi/com/ptz.cgi?continuouszoommove=0");
        Logger.trace("Stop zoom " + current_zoom + " (" + result + ")");
      } else {
        this.moveNotSupported(0);
      }
    } catch (IOException e) {
      Logger.error("stopZoom: " + e.getMessage());
    }
  }

  /**
   * Zoom in the camera at this speed.
   *
   * @param speed
   *            speed to zoom
   */
  @Override
  public void zoomIn(int speed) {
    try {

      if (this.can_move_continuous) {

        speed = (new Limits(0, lim_zoom.max())).clamp(speed);

        if (speed > 100)
          speed = 100;

        String result = doCommand("/axis-cgi/com/ptz.cgi?continuouszoommove=" + speed);
        target_zoom = lim_zoom.max();
        Logger.trace("Zoom in [" + speed + "] (" + result + ")");
      } else {
        this.moveNotSupported(speed);
      }
    } catch (IOException e) {
      Logger.error("zoomIn: " + e.getMessage());
    }
  }

  /**
   * Zoom out the camera at this speed.
   *
   * @param speed
   *            speed to zoom
   */
  @Override
  public void zoomOut(int speed) {
    try {

      if (this.can_move_continuous) {

        // negative values zoom out
        speed = (new Limits(0, lim_zoom.max())).clamp(speed) * -1;

        if (speed < -100)
          speed = -100;

        String result = doCommand("/axis-cgi/com/ptz.cgi?continuouszoommove=" + speed);
        target_zoom = lim_zoom.min();
        Logger.trace("Zoom out [" + speed + "] (" + result + ")");
      } else {
        this.moveNotSupported(speed);
      }
    } catch (IOException e) {
      Logger.error("zoomOut: " + e.getMessage());
    }
  }

  /**
   * Set the zoom level of the camera
   *
   * @param zoom
   *            set the zoom to this level
   */
  @Override
  public void zoom(int zoom) {

    Logger.debug("Zoom to " + zoom);

    try {
      String result = doCommand("/axis-cgi/com/ptz.cgi?zoom=" + zoom);
      target_zoom = zoom;
      Logger.trace("Zoom to " + zoom + " (" + result + ")");
    } catch (IOException e) {
      Logger.error("zoom: " + e.getMessage());
    }
  }

  /**
   * Return the value of the cameras current zoom level
   *
   * @return Value of the current zoom level
   */
  @Override
  public int getZoom() {

    Logger.trace("get zoom" + this.current_zoom);
    return this.current_zoom;
  }

  /**
   * Set the focus value for the camera
   *
   * @param focus
   *            The focus level to set the camera to
   */
  @Override
  public void focus(int focus) {

    try {
      String result = doCommand("/axis-cgi/com/ptz.cgi?focus=" + focus);
      current_focus = focus;
      Logger.trace("Set focus to " + focus + " (" + result + ")");
    } catch (IOException e) {
      Logger.error("focus: " + e.getMessage());
    }
  }

  /**
   * Return the focus value for the camera
   *
   * @return Value of the current focus level
   */
  @Override
  public int getFocus() {

    Logger.trace("Get focus " + current_focus);
    return  current_focus;
  }

  /**
   * Focus mode
   */
  @Override
  public void focusMode(FocusMode mode) {

  }

  /**
   * Return the current position of the camera
   *
   * @return Position of camera.
   */
  @Override
  public Position getPosition() {

    return current_pos;
  }

  /**
   * Add camera listener to list of observers
   *
   * @param listener
   *            CameraListener to add to this cameras list to notify
   */
  @Override
  public void addCameraListener(CameraListener listener) {
    if (listener != null) {
      observers.add(listener);
    } else {
      Logger.warn("VAPIX: null CameraListener");
    }
  }

  /**
   * Remove the camera listener from the list of observers
   *
   * @param listener
   *            CameraListener to remove from list to notify
   */
  @Override
  public void removeCameraListener(CameraListener listener) {
    if (listener != null) {
      observers.remove(listener);
    }
  }

  /**
   * Notify all camera listeners of cameras current position
   */
  void notifyCameraListeners() {
    Position pos = getPosition();
    for (CameraListener cl : observers) {
      if (cl != null)
        cl.positionUpdated(pos);
    }
  }

  /**
   * Map a value as sent through from camera operator to the range defined by
   * the config and then to onvif SOAP (val - in_min) * (out_max - out_min) /
   * (in_max - in_min) + out_min
   *
   * @param val
   *            value to map to the scaled range
   * @param in_min
   *            minimum input value
   * @param delta
   *            delta scale value difference between input and output
   * @param out_min
   *            minimum output value
   * @return mapped value
   */
  private float map(int val, int in_min, float delta, float out_min) {
    // return (val - in_min) * (out_max - out_min) / (in_max - in_min) +
    // out_min;
    float x = (float) (val - in_min);
    return (x * delta) + out_min;
  }

  /**
   * Map a value as sent through from camera operator to the range defined by
   * the config and then to onvif SOAP (val - in_min) * (out_max - out_min) /
   * (in_max - in_min) + out_min
   *
   * @param val
   *            value to map to the scaled range
   * @param in_min
   *            minimum input value
   * @param delta
   *            delta scale value difference between input and output
   * @param out_min
   *            minimum output value
   * @return mapped value
   */
  private float map(float val, int in_min, float delta, float out_min) {
    // return (val - in_min) * (out_max - out_min) / (in_max - in_min) +
    // out_min;
    float x = val - (float) in_min;
    return (x * delta) + out_min;
  }

  /**
   * Display error message for a move that is not supported
   *
   * @param a
   *            Value of input
   */
  private void moveNotSupported(int a) {

    Logger.warn("Move not supported:" + this.can_move_absolute + ":" + this.can_move_relative + ":"
        + this.can_move_continuous + " (" + a + ")");
  }

  /**
   * Display error message for a move that is not supported
   *
   * @param a
   *            Value of input
   * @param b
   *            Value of input
   */
  private void moveNotSupported(int a, int b) {

    Logger.warn("Move not supported:" + this.can_move_absolute + ":" + this.can_move_relative + ":"
        + this.can_move_continuous + " (" + a + "," + b + ")");
  }

  /**
   * Display error message for a move that is not supported
   *
   * @param a
   *            Value of input
   * @param b
   *            Value of input
   * @param target
   *            Value of target for move
   */
  private void moveNotSupported(int a, int b, Position target) {

    Logger.warn("Move not supported:" + this.can_move_absolute + ":" + this.can_move_relative + ":"
        + this.can_move_continuous + " (" + a + "," + b + ")" + " to [" + target.getX() + ";" + target.getY()
        + "]");
  }

  /**
   * Threads that frequently sends inquiries to all registered cameras.
   *
   */
  class CameraStateUpdater implements Runnable {

    @Override
    public void run() {

      int dx;
      int dy;
      int panSpeed;
      int tiltSpeed;
      if (running) {
        synchronized (mutex) {
          if (Logger.getLevel() == Level.TRACE) {
            int vpan = Math.round(map(current_panSpeed, speed_pan.min(), scale_pan_speed, 0));
            int vtilt = Math.round(map(current_tiltSpeed, speed_tilt.min(), scale_tilt_speed, 0));
            float x = map(current_pos.getX(), lim_pan.min(), scale_pan, range_pan.min());
            float y = map(current_pos.getY(), lim_tilt.min(), scale_tilt, range_tilt.min());
            float tx = map(target_pos.getX(), lim_pan.min(), scale_pan, range_pan.min());
            float ty = map(target_pos.getY(), lim_tilt.min(), scale_tilt, range_tilt.min());
            Logger.trace(String.format("VA: CUR %f %f, TAR %f %f, SPEED %d %d MODE %s",
                    x, y,
                    tx, ty,
                    vpan, vtilt,
                    current_mode));
          }

          dx = target_pos.getX() - current_pos.getX();
          dy = target_pos.getY() - current_pos.getY();

          if ("continuous".contentEquals(current_mode)) {
            panSpeed = Math.abs(current_panSpeed);
            tiltSpeed = Math.abs(current_tiltSpeed);
          } else {
            panSpeed = current_panSpeed;
            tiltSpeed = current_tiltSpeed;
          }
          // update X
          if (dx > 0) {
            current_pos.setX(current_pos.getX() + panSpeed);
            if (current_pos.getX() > target_pos.getX()) {
              current_pos.setX(target_pos.getX());
              current_panSpeed = 0;
            }
          } else if (dx < 0) {
            current_pos.setX(current_pos.getX() - panSpeed);
            if (current_pos.getX() < target_pos.getX()) {
              current_pos.setX(target_pos.getX());
              current_panSpeed = 0;
            }
          } else {
            current_panSpeed = 0;
          }

          // update Y
          if (dy > 0) {
            current_pos.setY(current_pos.getY() + tiltSpeed);
            if (current_pos.getY() > target_pos.getY()) {
              current_pos.setY(target_pos.getY());
              current_tiltSpeed = 0;
            }
          } else if (dy < 0) {
            current_pos.setY(current_pos.getY() - tiltSpeed);
            if (current_pos.getY() < target_pos.getY()) {
              current_pos.setY(target_pos.getY());
              current_tiltSpeed = 0;
            }
          } else {
            current_tiltSpeed = 0;
          }
        }
      }

      try {
        Logger.trace("Requesting camera position update");
        notifyCameraListeners();
      } catch (Exception e) {
        throw new IllegalStateException("Exception running camera state updater", e);
      }
    }
  }

}
